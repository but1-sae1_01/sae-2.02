"""
Graph problem, part1, common functions/variables
"""

"""
users (list of strings)
"""
users = []

"""
topics (list of strings)
"""
topics = []


def load_users():
    """
    load users from `users.txt` file (supposed to be in the same directory)
    :return: (None)
    """
    data_file = open("users.txt", 'r')

    for line in data_file:
        users.append(line.strip())

    data_file.close()


def load_topics():
    """
    load topics from `topics.txt` file (supposed to be in the same directory)
    :return: (None)
    """
    data_file = open("topics.txt", 'r')

    for line in data_file:
        topics.append(line.strip())

    data_file.close()

load_users()
load_topics()