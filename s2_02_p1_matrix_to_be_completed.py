# SAE S2.02
# Partie 1
# Travail par groupe

# NOM, Prénom et groupe de l'étudiant 1 : LAHYA Nadia, TD2
# NOM, Prénom et groupe de l'étudiant 2 : DIFI Camélia, TD2
# NOM, Prénom et groupe de l'étudiant 3 : BOULANOUAR Silia, TD3
# NOM, Prénom et groupe de l'étudiant 4 : MEHDI Kylian, TD1

# Modélisation avec matrices


"""
Graph problem, part1, implementation using successors matrices
"""
from s2_02_p1_common import *
from numpy import *

"""
following relationships 
matrix where user indexes are same as in `users` 
"""
following_relationships = []

"""
interests 
matrix where user/topic indexes are same as in `users` and `topics`
"""
interests = []


def load_following_relationships():
    """
    load following relationships from `following_relationships.csv` file (supposed to be in the same directory)
    :return: (None)
    """
    number_of_users = len(users)

    #Initialisation matrice
    global following_relationships
    following_relationships = [ [0] * number_of_users for k in range(number_of_users) ]

    data_file = open("following_relationships.csv", 'r')

    for line in data_file:

        line = line.strip()

        if line:
            user, friend = line.split(",")
            #  -------------------
            for i in range(number_of_users):
                for j in range(number_of_users):
                    if (users[i] == user and users[j] == friend):
                        following_relationships[i][j] = 1  
            #  -------------------

    data_file.close()
load_following_relationships()

def load_interests():
    """
    load interests from `interests.csv` file (supposed to be in the same directory)
    :return: (None)
    """
    number_of_users = len(users)
    number_of_topics = len(topics)

    #Initialisation matrice
    global interests
    interests = [ [0] * number_of_topics for k in range(number_of_users) ]

    data_file = open("interests.csv", 'r')

    for line in data_file:

        line = line.strip()

        if line:
            user, topic = line.split(",")
            #  -------------------
            for i in range(number_of_users):
                for j in range(number_of_topics):
                    if (users[i] == user and topics[j] == topic):
                        interests[i][j] = 1  
            #  -------------------
    
    data_file.close()
load_interests()

def transpose(matrix):
    """
    transpose matrix
    :param matrix: (list[list[int]]) matrix
    :return: (list[list[int]]) transposed matrix
    """
    global transposed_matrix
    transposed_matrix = []

    #  -------------------
    l = len(matrix)
    k = len(matrix[0])
    transposed_matrix =[ [0] * l for i in range(k) ]
    for x in range(l):
        for y in range(k):
            transposed_matrix[y][x] = matrix[x][y]
    return transposed_matrix
    
    #  -------------------


def boolean_product(matrix1, matrix2):
    """
    boalean matrix product
    :param matrix1: (list[list[int]]) first matrix
    :param matrix2: (list[list[int]]) second matrix
    :return: (list[list[int]]) matrix1 * matrix2, or [] if dimensions are invalid
    """
    global product_matrix
    product_matrix = []

    #  -------------------
    l=len(matrix1)
    resultats=[]
    for k in range(l):   
        resultats.append(matrix1[k] and matrix2[k])
    product_matrix = resultats[0] or resultats[1]
    if product_matrix == 1:
        return 1
    else:
        for i in range(1,l):
            product_matrix = product_matrix or resultats[i]
    return product_matrix
    #  -------------------


def get_followers_of(followee):
    """
    get followers of a specific user
    :param followee: (str) a user
    :return: (list[str]) a sorted list, containing all followers of `followee` (without duplicate)
    """
    followers = []

    #  -------------------
    if followee not in users:
        return 'User not found'
    else:
        followee_index = users.index(followee)
        for i in range(len(following_relationships)):
            if following_relationships[i][followee_index] == 1 and users[i] not in followers:
                followers.append(users[i])

    #  -------------------
    return sorted(followers)

def get_users_interested_in(topic):
    """
    get users interested in a specific topic
    :param topic: (str) a topic
    :return: (list[str]) a sorted list, containing all users interested in `topic` (without duplicate)
    """
    interested_users = []
    #  -------------------
    topicPosition = 0
    for i in range (len(topics)):
        if topics[i] == topic:
            topicPosition = i
    for i in range(len(interests)):
        if interests[i][topicPosition] == 1 and users[i] not in interested_users:
            interested_users.append(users[i])
    #  -------------------
    return sorted(interested_users)


def get_topics_in_descending_order_of_popularity():
    """
    get topics in descending order of popularity (number of interested users)
    :return: (list[(str, int)]) a list of tuples (topic, number of interested users), sorted in reverse order of popularity
    """
    global topics_in_descending_order_of_popularity
    topics_in_descending_order_of_popularity = []
    
    load_interests()
    transposed = transpose(interests)
    for i in range(len(transposed)):
        actual_interest_count = 0
        for k in transposed[i]:
            if k == 1:
                actual_interest_count += 1
        topics_in_descending_order_of_popularity.append((topics[i], actual_interest_count))
    topics_in_descending_order_of_popularity.sort(key=lambda x: x[1], reverse=True)
    return topics_in_descending_order_of_popularity

def get_most_popular_topic():
    """
    get most popular topic (greatest number of interested users)
    :return: (str) most popular topic. N.B. in case of equality, meaning several topics with the same greatest number of
    interested users, the function should return the first element of the sorted list of these topics.
    """
    most_popular_topics = []
    get_topics_in_descending_order_of_popularity()
    
    for i in range(len(topics_in_descending_order_of_popularity)):
        if topics_in_descending_order_of_popularity[i][1] == topics_in_descending_order_of_popularity[0][1]:
            most_popular_topics.append(topics_in_descending_order_of_popularity[i][0])
    most_popular_topics = sorted(most_popular_topics)

    return most_popular_topics[0]


def get_most_popular_user():
    """
    get most popular user (user with the greatest number of followers)
    :return: (str) most popular user. N.B. in case of equality, meaning several users with the same greatest number of
    followers, the function should return the first element of the sorted list of these users.
    """
    most_popular_users_descending_order = []
    most_popular_users = []

    #  -------------------
    transposed = transpose(following_relationships)
    for i in range(len(transposed)):
        actual_followers_count = 0
        for k in transposed[i]:
            if k == 1:
                actual_followers_count += 1
        most_popular_users_descending_order.append((users[i], actual_followers_count))
    most_popular_users_descending_order.sort(key=lambda x: x[1], reverse=True)

    for i in range(len(most_popular_users_descending_order)):
        if most_popular_users_descending_order[i][1] == most_popular_users_descending_order[0][1]:
            most_popular_users.append(most_popular_users_descending_order[i][0])
    most_popular_users = sorted(most_popular_users)
    
    return most_popular_users[0]

def get_repost_viewers(user):
    """
    get viewers of a repost, for a post originating by a specific user. Is considered as a viewer of this repost,
    a user following a user following the specific user from which this post originates
    :param user: (str) a user
    :return: (list[str]) sorted list of users viewing this post (without duplicates, specific user and its followers excluded).
    if no reviewers are found, the function should return `["<no solution>"]`
    """
    second_order_followers = set()
    first_order_followers = get_followers_of(user)

    if user not in users:
        return ["<no solution>"]
    
    for follower in first_order_followers:
        followers_of_follower = get_followers_of(follower)
        for second_order_follower in followers_of_follower:
            if second_order_follower != user and second_order_follower not in first_order_followers:
                second_order_followers.add(second_order_follower)

    if not second_order_followers:
        return ["<no solution>"]
    return sorted(list(second_order_followers))
