# SAE S2.02
# Partie 1
# Travail par groupe

# NOM, Prénom et groupe de l'étudiant 1 : LAHYA Nadia, TD2
# NOM, Prénom et groupe de l'étudiant 2 : DIFI Camélia, TD2
# NOM, Prénom et groupe de l'étudiant 3 : BOULANOUAR Silia, TD3
# NOM, Prénom et groupe de l'étudiant 4 : MEHDI Kylian, TD1

# Modélisation avec listes


"""
Graph problem, part1, implementation using LS and TS lists
"""
from s2_02_p1_common import *
from numpy import *
"""
following relationships 
lists where user indexes are same as in `users` 
"""
#  -------------------
following_relationships_LS = []
following_relationships_TS = []
#  -------------------

"""
interests 
lists where user/topic indexes are same as in `users` and `topics`
"""
#  -------------------
interests_LS = []
interests_TS = []
#  -------------------

def load_following_relationships():
    """
    load following relationships from `following_relationships.csv` file (supposed to be in the same directory)
    :return: (None)
    """
    global following_relationships_LS
    global following_relationships_TS
    following = {user :[] for user in users}

    data_file = open("following_relationships.csv", 'r')

    for line in data_file:
        line = line.strip()
        if line:
            user, friend = line.split(",")
            if user in following:
                following[user].append(friend) 

    data_file.close()

    for user in users:
        following[user] = sorted(following[user])

    following_relationships_TS = [1]
    current_index = 1

    for user in users:
        friends = following[user]
        following_relationships_LS.extend([users.index(friend) + 1 for friend in friends])
        current_index += len(friends)
        following_relationships_TS.append(current_index)

    if following_relationships_TS[-1] == following_relationships_TS[-2]:
        following_relationships_TS.pop()

load_following_relationships()


def load_interests():
    """
    load interests from `interests.csv` file (supposed to be in the same directory)
    :return: (None)
    """
    global interests_TS
    global interests_LS
    interests = {topic :[] for topic in topics}


    data_file = open("interests.csv", 'r')

    for line in data_file:
        line = line.strip()
        if line:
            user, topic = line.split(",")
            if topic in interests:
                interests[topic].append(user)

    for topic in topics:
        interests[topic] = sorted(interests[topic])

    interests_TS = [1]
    current_index = 1

    for topic in topics:
        users_interested = interests[topic]
        interests_LS.extend([users.index(user) + 1 for user in users_interested])
        current_index += len(users_interested)
        interests_TS.append(current_index)

    if interests_TS[-1] == interests_TS[-2]:
        interests_TS.pop()

    data_file.close()
load_interests()


def get_followers_of(followee):
    """
    get followers of a specific user
    :param followee: (str) a user
    :return: (list[str]) a sorted list, containing all followers of `followee` (without duplicate)
    """
    followers = []
    for i in range(len(following_relationships_TS) - 1):
        start_index = following_relationships_TS[i]
        end_index = following_relationships_TS[i + 1] - 1
        followee_index = users.index(followee) + 1
        if followee_index in following_relationships_LS[start_index-1:end_index] and users[i] not in followers:
            followers.append(users[i])

    followers.sort()
    
    return list(followers)

def get_users_interested_in(topic):
    """
    get users interested in a specific topic
    :param topic: (str) a topic
    :return: (list[str]) a sorted list, containing all users interested in `topic` (without duplicate)
    """
    interested_users = []

    topic_position = topics.index(topic)
    start = interests_TS[topic_position]
    end = interests_TS[topic_position + 1] - 1 if topic_position + 1 < len(interests_TS) else len(interests_LS)

    for i in range(start - 1, end):
        interested_users.append(users[interests_LS[i] - 1])
    return sorted(list(set(interested_users)))

get_users_interested_in("GRASS")

def get_topics_in_descending_order_of_popularity():
    """
    get topics in descending order of popularity (number of interested users)
    :return: (list[(str, int)]) a list of tuples (topic, number of interested users), sorted in reverse order of popularity
    """
    global topics_in_descending_order_of_popularity
    topics_in_descending_order_of_popularity = []
    L =[]

    for topic in topics:
        L.append(len(get_users_interested_in(topic)))
    
    while L:
       maximum = max(L) 
       index = L.index(maximum)

       topics_in_descending_order_of_popularity.append(topics[index])
       L.pop(index)
       topics.pop(index)

    return topics_in_descending_order_of_popularity

get_topics_in_descending_order_of_popularity()

def get_most_popular_topic():
    """
    get most popular topic (greatest number of interested users)
    :return: (str) most popular topic. N.B. in case of equality, meaning several topics with the same greatest number of
    interested users, the function should return the first element of the sorted list of these topics.
    """
    most_popular_topics = []
    for i in range(len(topics_in_descending_order_of_popularity)):
        most_popular_topics.append(topics_in_descending_order_of_popularity[i])

    return most_popular_topics[0]
get_most_popular_topic()

def get_most_popular_user():
    """
    get most popular user (user with the greatest number of followers)
    :return: (str) most popular user. N.B. in case of equality, meaning several users with the same greatest number of
    followers, the function should return the first element of the sorted list of these users.
    """
    most_popular_users = []
    L= []
          
    for user in users:
        L.append(len(get_followers_of(user)))
    
    max = L[0]
    for i in range(len(L)):
        if (max <= L[i]) and (users[i] not in most_popular_users):
            max = L[i]
            most_popular_users.append(users[i])
    most_popular_users = sorted(most_popular_users)
    
    return most_popular_users[0]

def get_repost_viewers(user):
    """
    get viewers of a repost, for a post originating by a specific user. Is considered as a viewer of this repost,
    a user following a user following the specific user from which this post originates
    :param user: (str) a user
    :return: (list[str]) sorted list of users viewing this post (without duplicates, specific user and its followers excluded).
    if no reviewers are found, the function should return `["<no solution>"]`
    """
    second_order_followers = set()

    direct_followers = get_followers_of(user)
    second_order_followers = set()

    for follower in direct_followers:
        follower_followers = get_followers_of(follower)
        second_order_followers.update(follower_followers)

    second_order_followers.difference_update(set(direct_followers))
    second_order_followers.discard(user)

    if not second_order_followers:
        return ["<no solution>"]

    return sorted(list(second_order_followers))
    #  -------------------
