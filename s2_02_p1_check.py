"""
SAE S2.02 part1 code validator

This script intends to validate function implementations done in `s2_02_p1_list.py` and  `s2_02_p1_matrix.py`
(scripts supposed to be in the same directory).

In order to validate either one script or another, simply uncomment the relevant import.
"""
# uncomment only this import in order to validate matrix version
#from s2_02_p1_matrix_to_be_completed import *

# uncomment only this import in order to validate list version
#from s2_02_p1_list_to_be_completed import *

# uncomment only this import in order to validate dictionary version
#from s2_02_p1_dictionary_to_be_completed import *

""" 
expected results for question 1
"""
e1 = {'Katie White': ['Elizabeth Hughes', 'Jennifer Perez', 'Lauren Hawkins', 'Lisa Russell'],
      'Jennifer Perez': ['Christian Taylor'],
      'Troy Harrell': ['Jennifer Perez', 'Lauren Hawkins', 'Michael Snyder DDS'],
      'Robert Holland': ['Eric Snyder', 'Lisa Russell'],
      'Lisa Russell': ['Eric Snyder', 'Troy Harrell'],
      'Michael Fisher': [],
      'Austin Boyer': ['Elizabeth Hughes', 'Jennifer Perez', 'Jessica Mills', 'Madison Wright'],
      'Lauren Hawkins': ['Eric Snyder', 'Michael Fisher']
      }

""" 
expected results for question 2
"""
e2 = {'GRASS': ['Jennifer Perez', 'Kim Taylor'],
      'Janus': ['Christian Taylor', 'Jessica Mills', 'John Stein', 'Kim Taylor'],
      'Lingo': [],
      'RPL': ['Anthony Russell', 'Kim Taylor'],
      'Claire': ['Mrs. Mary Johnson'],
      'Gosu': ['Austin Boyer', 'Michael Fisher', 'Mrs. Mary Johnson'],
      'rc': ['Christian Taylor', 'Jennifer Perez', 'John Stein', 'Jose Gonzalez'],
      'Hamilton C shell': ['Austin Boyer', 'John Stein']
      }

""" 
expected results for question 3
"""
e3 = 'Janus'

""" 
expected results for question 4
"""
e4 = 'Austin Boyer'

""" 
expected results for question 5
"""
e5 = {'Katie White': ['Christian Taylor', 'Eric Snyder', 'Michael Fisher', 'Troy Harrell'],
      'Jennifer Perez': ['Anthony Russell', 'Jessica Mills', 'Lisa Russell'],
      'Troy Harrell': ['Christian Taylor', 'Eric Snyder', 'Michael Fisher'],
      'Robert Holland': ['Madison Wright', 'Troy Harrell'],
      'Lisa Russell': ['Jennifer Perez', 'Lauren Hawkins', 'Madison Wright', 'Michael Snyder DDS'],
      'Michael Fisher': ["<no solution>"],
      'Austin Boyer': ['Caleb Perez', 'Christian Taylor', 'Katie White'],
      'Lauren Hawkins': ['Madison Wright', 'Troy Harrell']
      }


def load_data():
    """
    load data from txt/csv files (supposed to be in the same directory)
    :return: (None)
    """
    load_users()

    load_topics()

    load_following_relationships()

    load_interests()


def validate():
    """
    validate list/matrix function implementations, printing PASSED/FAILED for each test case
    as well as expected vs returned if FAILED.

    :return: (None)
    """
    load_data()

    print("QUESTION 1")
    validate_function(get_followers_of, e1, users[0:8])

    print()
    print("QUESTION 2")
    validate_function(get_users_interested_in, e2, topics[0:8])

    print()
    print("QUESTION 3")
    validate_function(get_most_popular_topic, e3)

    print()
    print("QUESTION 4")
    validate_function(get_most_popular_user, e4)

    print()
    print("QUESTION 5")
    validate_function(get_repost_viewers, e5, users[0:8])


def validate_function(function, expected, input_list=None):
    """
    validate a specific function
    :param function: function to check
    :param expected: expected result
    :param input_list: list containing inputs (test cases)
    :return: (None)
    """

    returned = {}
    if input_list is None:
        returned = function()
        if returned == expected:
            print("(PASSED)")
        else:
            print("(FAILED)")
            print("expected: ", expected)
            print("returned: ", returned)
    else:
        for element in input_list:
            returned[element] = function(element)
            result = "(PASSED)" if returned[element] == expected[element] else "(FAILED)"
            print(result, end='\t')
            print(element)
            if result == "(FAILED)":
                print("expected: ", expected[element])
                print("returned: ", returned[element])


validate()
