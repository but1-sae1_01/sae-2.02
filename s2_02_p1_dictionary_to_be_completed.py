# SAE S2.02
# Partie 1
# Travail par groupe

# NOM, Prénom et groupe de l'étudiant 1 : LAHYA Nadia, TD2
# NOM, Prénom et groupe de l'étudiant 2 : DIFI Camélia, TD2
# NOM, Prénom et groupe de l'étudiant 3 : BOULANOUAR Silia, TD3
# NOM, Prénom et groupe de l'étudiant 4 : MEHDI Kylian, TD1

# Modélisation avec dictionnaires


"""
Graph problem, part1, implementation using dictionaries
"""
from s2_02_p1_common import *
from numpy import *

"""
following relationships, a dictionary
 
"""
following_relationships = {}

"""
interests, a dictionary

"""
interests = {}


def load_following_relationships():
    """
    load following relationships from `following_relationships.csv` file (supposed to be in the same directory)
    :return: (None)
    """
    number_of_users = len(users)
    
    #  -------------------
    for i in range(number_of_users):
        following_relationships[users[i]] = []
    #  -------------------

    data_file = open("following_relationships.csv", 'r')

    for line in data_file:

        line = line.strip()
        if line:
            user, friend = line.split(",")
            #  -------------------
            following_relationships[user].append(friend)
            #  -------------------
    data_file.close()

load_following_relationships()


def load_interests():
    """
    load interests from `interests.csv` file (supposed to be in the same directory)
    :return: (None)
    """
    number_of_users = len(users)

    for i in range(number_of_users):
        interests[users[i]] = []
 
    data_file = open("interests.csv", 'r')

    for line in data_file:

        line = line.strip()

        if line:
            user, topic = line.split(",")
            interests[user].append(topic)
            
    data_file.close()
   
def get_followers_of(followee):
    """
    get followers of a specific user
    :param followee: (str) a user
    :return: (list[str]) a sorted list, containing all followers of `followee` (without duplicate)
    """
    followers = []
    
    for user, friends in following_relationships.items():
        if followee in friends:
            followers.append(user)
    return sorted(followers)

def get_users_interested_in(topic):
    """
    Get users interested in a specific topic
    :param topic: (str) a topic
    :return: (list[str]) a sorted list, containing all users interested in `topic` (without duplicates)
    """
    interested_users = []

    for user, interest in interests.items():
        for i in range (len(interest)):
            if interest[i] == topic:
                interested_users.append(user)

    return sorted(interested_users)

def get_topics_in_descending_order_of_popularity():
    """
    get topics in descending order of popularity (number of interested users)
    :return: (list[(str, int)]) a list of tuples (topic, number of interested users), sorted in reverse order of popularity
    """
    global topics_in_descending_order_of_popularity
    topics_in_descending_order_of_popularity = []
    topic_popularity = {}

    for user, topics in interests.items():
        for k in topics:
            if k in topic_popularity:
                topic_popularity[k] += 1
            else:
                topic_popularity[k] = 1

    topics_in_descending_order_of_popularity = sorted(topic_popularity.items(), key=lambda x: x[1], reverse=True)

    return topics_in_descending_order_of_popularity

def get_most_popular_topic():
    """
    get most popular topic (greatest number of interested users)
    :return: (str) most popular topic. N.B. in case of equality, meaning several topics with the same greatest number of
    interested users, the function should return the first element of the sorted list of these topics.
    """
    most_popular_topics = []
    get_topics_in_descending_order_of_popularity()

    for k in topics_in_descending_order_of_popularity:
        if k[1] == topics_in_descending_order_of_popularity[0][1] and k[1] not in most_popular_topics:
            most_popular_topics.append(k[0])
    most_popular_topics =  sorted(most_popular_topics)

    return most_popular_topics[0]

def get_most_popular_user():
    """
    get most popular user (user with the greatest number of followers)
    :return: (str) most popular user. N.B. in case of equality, meaning several users with the same greatest number of
    followers, the function should return the first element of the sorted list of these users.
    """
    most_popular_users = []
    user_popularity = {user: len(get_followers_of(user)) for user in users}
    users_in_descending_order_of_popularity = sorted(user_popularity.items(), key=lambda x: x[1], reverse=True)
    
    for k in users_in_descending_order_of_popularity:
        if k[1] == users_in_descending_order_of_popularity[0][1] and k[1] not in most_popular_users:
            most_popular_users.append(k[0])
    most_popular_users =  sorted(most_popular_users)

    return most_popular_users[0]

def get_repost_viewers(user):
    """
    get viewers of a repost, for a post originating by a specific user. Is considered as a viewer of this repost,
    a user following a user following the specific user from which this post originates
    :param user: (str) a user
    :return: (list[str]) sorted list of users viewing this post (without duplicates, specific user and its followers excluded).
    if no reviewers are found, the function should return `["<no solution>"]`
    """
    
    second_order_followers = set()
    first_order_followers = get_followers_of(user)
    #  -------------------
    if user not in users:
        return ["<no solution>"]

    for i in first_order_followers:
        if i in following_relationships:
            second_order = get_followers_of(i)
            for j in second_order:
                if j != user and j not in second_order_followers and j not in first_order_followers:
                    second_order_followers.add(j)
    #  -------------------
    if second_order_followers == set():
        return ["<no solution>"]
    
    return sorted(list(second_order_followers))
